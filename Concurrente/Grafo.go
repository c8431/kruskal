package main

import (
	"fmt"
	"sort"
)

type Vertice interface {
	comparable
}

type Arista[T Vertice] struct {
	Inicio T
	Fin    T
	Coste  float64
}

type SemiArista[T Vertice] struct {
	Fin   T
	Coste float64
}

type Grafo[T Vertice] struct {
	Adjacencia map[T]*Set[SemiArista[T]]
	Dirigido   bool
}

func NuevoGrafo[T Vertice]() *Grafo[T] {
	return &Grafo[T]{
		Adjacencia: map[T]*Set[SemiArista[T]]{},
		Dirigido:   false,
	}
}

func (g *Grafo[T]) AñadirVertice(v T) {
	if _, exists := g.Adjacencia[v]; !exists {
		g.Adjacencia[v] = CrearSetDatos[SemiArista[T]]()
	}
}

func (g *Grafo[T]) AñadirArista(v1, v2 T, c float64) {
	g.AddVertex(v1)
	g.AddVertex(v2)

	g.Adjacencia[v1].Añadir(SemiArista[T]{
		Fin:   v2,
		Coste: c,
	})

	if !g.Dirigido {
		g.Adjacencia[v2].Añadir(SemiArista[T]{
			Fin:   v1,
			Coste: c,
		})
	}
}

func (g *Grafo[T]) Mostrar() {
	g.EachEdge(func(e Arista[T], _ func()) {
		fmt.Printf("(%v,%v,%f)\n", e.Inicio, e.Fin, e.Coste)
	})
}

type AristOrdenadas[T Vertice] []Arista[T]

func (se AristOrdenadas[T]) Len() int {
	return len(se)
}

func (se AristOrdenadas[T]) Less(i, j int) bool {
	return se[i].Coste < se[j].Coste
}

func (se AristOrdenadas[T]) Swap(i, j int) {
	se[i], se[j] = se[j], se[i]
}

// EachEdge calls f for every edge.
func (g *Grafo[T]) EachEdge(f func(Arista[T], func())) {
	var stopped bool
	stop := func() { stopped = true }
	for v, s := range g.Adjacencia {
		s.ParaCadaElem(func(he SemiArista[T], innerStop func()) {
			edge := Arista[T]{v, he.Fin, he.Coste}
			f(edge, stop)
			if stopped {
				innerStop()
			}
		})
		if stopped {
			break
		}
	}
}

func (g *Grafo[T]) TodasSemiAristas(v T, f func(SemiArista[T], func())) {
	if s, exists := g.Adjacencia[v]; exists {
		var done bool
		Acabar := func() { done = true }
		s.ParaCadaElem(func(SA SemiArista[T], isDone func()) {
			f(SA, Acabar)
			if done {
				isDone()
			}
		})
	}
}

func (g *Grafo[T]) AristOrdenadas() AristOrdenadas[T] {
	set := CrearSetVacio[Arista[T]]()

	for v := range g.Adjacencia {
		g.TodasSemiAristas(v, func(he SemiArista[T], _ func()) {
			set.Añadir(Arista[T]{
				Inicio: v,
				Fin:    he.Fin,
				Coste:  he.Coste,
			})
		})
	}

	aristas := make(AristOrdenadas[T], set.Len())
	set.ParaCadaElem(func(e Arista[T], _ func()) {
		aristas = append(aristas, e)
	})

	sort.Sort(&aristas)
	return aristas
}

func (g *Grafo[T]) AddVertex(v T) {
	if _, exists := g.Adjacencia[v]; !exists {
		g.Adjacencia[v] = CrearSetVacio[SemiArista[T]]()
	}
}
