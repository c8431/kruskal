package main

func main() {

	grafo := NuevoGrafo[string]()

	grafo.AñadirArista("A", "B", 8)
	grafo.AñadirArista("A", "C", 7)
	grafo.AñadirArista("B", "D", 4)
	grafo.AñadirArista("B", "C", 6)
	grafo.AñadirArista("C", "D", 9)
	grafo.AñadirArista("D", "B", 5)
	grafo.AñadirArista("D", "E", 3)
	grafo.AñadirArista("E", "C", 9)
	grafo.AñadirArista("E", "A", 2)

	tree := MainKruksal(grafo)

	tree.Mostrar()

}
