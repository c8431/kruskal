import os

#Inicializamos el nodo y el arreglo de conjuntos
Nodo = dict()
conjuntos = []

#Agregando los nodos
def Hacer_set(vertice):
    Nodo[vertice] = vertice

#Buscando los nodos
def Buscar_set(vertice):
    if Nodo[vertice] != vertice:
        Nodo[vertice] = Buscar_set(Nodo[vertice])
    return Nodo[vertice]

#Dividiendo Clusters y verificando
def Union(u, v, Lista_Ordenada):

    Dato1 = Buscar_set(u)
    Dato2 = Buscar_set(v)
    if Dato1 != Dato2:
        for Dato in Lista_Ordenada:
            Nodo[Dato1] = Dato2

def Kruskal(grafo):
    resultante = []
    cont = 0
    for vertice in grafo['A']:
        Hacer_set(vertice)

    Lista_Ordenada = list(grafo['B'])
    Lista_Ordenada.sort()
    Lista_Ordenada = [(a,b,c) for c,a,b in Lista_Ordenada]
    print ("==============================")
    print ("Datos Ordenados")
    print ("==============================")
    print ("Ordenados:",Lista_Ordenada)
    Lista_Ordenada = [(c,a,b) for a,b,c in Lista_Ordenada]
    for Dato in Lista_Ordenada:
        peso, u, v = Dato
        if Buscar_set(u) != Buscar_set(v):
            resultante.append(Dato)
            print ("==============================")
            print ("Paso:",cont)
            print ("==============================")
            resultante = [(a,b,c) for c,a,b in resultante]
            print ("Resultante: ",resultante)
            resultante = [(c,a,b) for a,b,c in resultante]
            cont+=1
            Union(u, v, Lista_Ordenada)

    return resultante

grafo = {
        'A': ['a','b','c','d','e','f','g','h','i','j','p'],
        'B': [(1, 'a', 'b'),
              (1, 'g', 'h'),
              (2, 'b', 'e'),
              (2, 'd', 'g'),
              (2, 'b', 'd'),
              (2, 'e', 'c'),
              (2, 'i', 'f'),
              (3, 'b', 'c'),
              (4, 'a', 'p'),
              (4, 'e', 'h'),
              (5, 'e', 'f'),
              (5, 'c', 'f'),
              (6, 'j', 'c'),
              (8, 'p', 'd'),
              (9, 'e', 'g'),
              (10, 'f', 'h'),
              (15, 'a', 'j'),
            ]
        }

resultante = Kruskal(grafo)

