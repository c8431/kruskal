#define wait(s) atomic {s>0 -> s--}
#define signal(s) s++

#define Vertice 10
#define Arista  9

byte VerticeCorrecto=0
byte MenorArista=Arista
byte MenorCantidadTotal=Vertice


active proctype BuscarArista(){

        do
        ::
           printf("Seccion no critica 1 : Comenzando busqueda de cualquierda arista (%d)) \n", _pid)
           printf("Seccion no critica 2 : Comenzando busqueda de cualquierda arista (%d)) \n", _pid)
           wait(MenorArista)
           printf("Seccion CRITICA : Seleccionando la menor arista de menor peso (%d)) \n ", _pid)
           printf("Seccion CRITICA : Seleccionando la menor arista de menor peso (%d)) \n ", _pid)
           signal(VerticeCorrecto)
           wait(MenorCantidadTotal)
        od

}
active proctype RecorrerVertice (){
        do
        ::
           printf ("Seccion no critica 1 : Sumando Aristas  \n", _pid)
           printf ("Seccion no critica 2 : Sumando Aristas \n", _pid)
           wait (VerticeCorrecto)
           printf ("Seccion CRITICA 1 : Expandiendo Arbol-grafo \n" , _pid)
           printf ("Seccion CRITICA 2 : Expandiendo Arbol-grafo \n" , _pid)
           signal(MenorArista)


        od
}

active proctype Kruskall (){
        printf("Kruskall completado \n")
        signal(MenorCantidadTotal)

}