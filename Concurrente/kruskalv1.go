package main

func MainKruksal[T Vertice](g *Grafo[T]) *Grafo[T] {
	Arbol := NuevoGrafo[T]()
	cc := map[T]int{}
	ccid := 1

	for _, arist := range g.AristOrdenadas() {
		if _, exists := cc[arist.Inicio]; !exists {
			cc[arist.Inicio] = ccid
			ccid++
		}

		if cc[arist.Inicio] == cc[arist.Fin] {
			continue
		}
		if cc[arist.Fin] != 0 {
			endid := cc[arist.Fin]
			for v, id := range cc {
				if id == endid {
					cc[v] = cc[arist.Inicio]
				}
			}
		} else {
			cc[arist.Fin] = cc[arist.Inicio]
		}
		Arbol.AñadirArista(arist.Inicio, arist.Fin, arist.Coste)
	}
	return Arbol
}
