package main

type Set[T comparable] map[T]struct{}

func CrearSetVacio[T comparable]() *Set[T] {
	return &Set[T]{}
}

func CrearSetDatos[T comparable](elements ...T) *Set[T] {
	set := CrearSetVacio[T]()
	for _, element := range elements {
		set.Añadir(element)
	}
	return set
}

func (s *Set[T]) Añadir(element T) bool {
	_, exists := (*s)[element]
	(*s)[element] = struct{}{}
	return !exists
}

func (s *Set[T]) Len() int {
	return len(*s)
}

func (s *Set[T]) Equivale(s2 *Set[T]) bool {
	if s2 == nil || s.Len() != s2.Len() {
		return false
	}
	for element, _ := range *s {
		if !s2.Contiene(element) {
			return false
		}
	}
	return true
}

func (s *Set[T]) Contiene(element T) bool {
	_, exists := (*s)[element]
	return exists
}

func (s *Set[T]) Unir(s2 *Set[T]) {
	if s2 == nil {
		return
	}
	for element, _ := range *s2 {
		s.Añadir(element)
	}
}

func (s *Set[T]) Remover(element T) bool {
	if _, exists := (*s)[element]; exists {
		delete(*s, element)
		return true
	}
	return false
}

func (s *Set[T]) ReturnTodo() T {
	for v, _ := range *s {
		return v
	}
	panic("grafo: empty set")
}

func (s *Set[T]) ParaCadaElem(f func(T, func())) {
	stopped := false
	stop := func() { stopped = true }
	for v, _ := range *s {
		f(v, stop)
		if stopped {
			return
		}
	}
}
